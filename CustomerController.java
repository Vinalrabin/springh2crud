package com.example.demo.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.customer;
import com.example.demo.repository.IcustomerRepo;

@RestController
public class CustomerController {
	
	@Autowired
	IcustomerRepo customerRepo;
	
	@PostMapping("/tbl_customer")
	public ResponseEntity<customer>save(@RequestBody customer cust)
	{
		try
		{
			return new ResponseEntity<>(customerRepo.save(cust), HttpStatus.CREATED);
		}
		catch(Exception e)
		{
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/tbl_customer")
public ResponseEntity<List<customer>> getAllcustomers()
{
	try
	{
		List<customer> tbl = customerRepo.findAll();
		if( tbl.isEmpty() || tbl.size() == 0)
		{
			return new ResponseEntity<List<customer>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<customer>>(tbl, HttpStatus.OK); 
	}
	catch (Exception e)
	{
		return new ResponseEntity<> (null, HttpStatus.NOT_FOUND);
	}
}
	
	@GetMapping("/tbl_customer/{id}")
	
	public ResponseEntity<customer> getSinglecustomer(@PathVariable Long id)
	{
		Optional<customer> Coustomer = customerRepo.findById(id);
		if(Coustomer.isPresent())
		{
			return new ResponseEntity<customer>(Coustomer.get(), HttpStatus.OK);
		}
		return new ResponseEntity<customer>(HttpStatus.NOT_FOUND);
	}
	
	@PutMapping("/tbl_customer/{id}")
	public ResponseEntity<?> UpdateById(@PathVariable("id") Long id,@RequestBody customer Coustomer)
	{
    	Optional<customer> customerOptional = customerRepo.findById(id);
    	if(customerOptional.isPresent()) {
    		customer CoustomerToSave = customerOptional.get();
    		CoustomerToSave.setName(Coustomer.getName()!=null ? Coustomer.getName() : CoustomerToSave.getName());
    		CoustomerToSave.setAge(Coustomer.getAge()!=null ? Coustomer.getAge() : CoustomerToSave.getAge());
    		CoustomerToSave.setLocation(Coustomer.getLocation()!=null ? Coustomer.getLocation() : CoustomerToSave.getLocation());
    		CoustomerToSave.setUpdatedAt(new Date(System.currentTimeMillis()));
    		customerRepo.save(CoustomerToSave);
    		return new ResponseEntity<>(CoustomerToSave, HttpStatus.OK);
	}
    	else {
        	return new ResponseEntity<>("Todo not found with id"+id, HttpStatus.NOT_FOUND);
    	}
	}
	
	@DeleteMapping("/tbl_customer/{id}")
	public ResponseEntity<?> deleteById(@PathVariable("id") Long id)
	{
		try{
            customerRepo.deleteById(id);
            return new ResponseEntity<>("Successfully deleted todo with id "+id, HttpStatus.OK);
        }
        catch (Exception e)
        {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }	
	}
	}
	
	


	
	

	
